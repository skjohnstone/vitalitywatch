package com.example.sjohn.vitalitywatchprototype;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by sjohn on 03/11/2017.
 */

public class ScoreList implements Serializable {
    private HashMap<Date, Score> scoreHashMap;
    private Score score;
    private Date date;

    public ScoreList(Date date) {
        this.scoreHashMap = new HashMap<Date, Score>();
        this.score = new Score(date);
        this.date = date;
        this.scoreHashMap.put(date, score);
    }

    public Score getScore() { return this.score; }

    public void setScore(Score score) {
        this.score = score;
        this.scoreHashMap.put(this.date, score);
    }

    public void scoreListDateChanged (Date date) {
        this.date = date;
        if (this.scoreHashMap.get(date) == null) {
            this.score = new Score(date);
            this.scoreHashMap.put(date, this.score);
        } else {
            this.score = this.scoreHashMap.get(date);
        }
    }
}
