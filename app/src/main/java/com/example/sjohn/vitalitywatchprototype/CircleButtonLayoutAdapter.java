package com.example.sjohn.vitalitywatchprototype;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by sjohn on 02/11/2017.
 */

public class CircleButtonLayoutAdapter {

    private final LayoutInflater inflater;
    private ArrayList<View>  buttonList;
    private ArrayList<String> buttonNames;
    private View background;

    public CircleButtonLayoutAdapter(LayoutInflater inflater) {
        this.buttonList = new ArrayList<>();
        this.buttonNames = new ArrayList<>();
        this.inflater = inflater;

        background = inflater.inflate(R.layout.circle_background_layout,null);

        View alcoholButton = inflater.inflate(R.layout.custom_button_layout, null);
        View bloodsButton = inflater.inflate(R.layout.custom_button_layout, null);
        View destressButton = inflater.inflate(R.layout.custom_button_layout, null);
        View detoxButton = inflater.inflate(R.layout.custom_button_layout, null);
        View dietButton = inflater.inflate(R.layout.custom_button_layout, null);
        View exerciseButton = inflater.inflate(R.layout.custom_button_layout, null);
        View fruitButton = inflater.inflate(R.layout.custom_button_layout, null);
        View happyButton = inflater.inflate(R.layout.custom_button_layout, null);
        View sleepButton = inflater.inflate(R.layout.custom_button_layout, null);
        View sugarSaltButton = inflater.inflate(R.layout.custom_button_layout, null);
        View waterButton = inflater.inflate(R.layout.custom_button_layout, null);
        View weightButton = inflater.inflate(R.layout.custom_button_layout, null);

        buttonList.add(alcoholButton);      //0
        buttonList.add(bloodsButton);       //1
        buttonList.add(destressButton);     //2
        buttonList.add(detoxButton);        //3
        buttonList.add(dietButton);         //4
        buttonList.add(exerciseButton);     //5
        buttonList.add(fruitButton);        //6
        buttonList.add(happyButton);        //7
        buttonList.add(sleepButton);        //8
        buttonList.add(sugarSaltButton);    //9
        buttonList.add(waterButton);        //10
        buttonList.add(weightButton);       //11

        buttonNames.add("Alcohol");         //0
        buttonNames.add("Bloods");          //1
        buttonNames.add("De-Stress");       //2
        buttonNames.add("De-Tox");          //3
        buttonNames.add("Diet");            //4
        buttonNames.add("Exercise");        //5
        buttonNames.add("5 a Day");         //6
        buttonNames.add("Happy Hours");     //7
        buttonNames.add("Sleep");           //8
        buttonNames.add("Sugar & Salt");  //9
        buttonNames.add("Water");           //10
        buttonNames.add("Weight");          //11
    }

    public View getBackground() { return this.background; }

    public ArrayList<View> getAllViews() { return this.buttonList; }

    public int getCount() { return this.buttonList.size(); }

    public View getViewAt(int i) { return this.buttonList.get(i); }

    public String getNameAt (int i) { return this.buttonNames.get(i);}
}
