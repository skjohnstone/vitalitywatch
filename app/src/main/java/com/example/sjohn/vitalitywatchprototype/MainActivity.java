package com.example.sjohn.vitalitywatchprototype;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity implements View.OnClickListener/*, NumberPicker.OnValueChangeListener*/  {
    private CircleButtonLayoutAdapter adapter;
    private SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
    private SimpleDateFormat dayFormatter = new SimpleDateFormat("dd");
    private SimpleDateFormat monthFormatter= new SimpleDateFormat ("MM");
    private SimpleDateFormat yearFormatter = new SimpleDateFormat("yyyy");
    public static int displayedDay;
    public static int displayedMonth;
    public static int displayedYear;
    private Calendar c;
    private Date currDate;
    private ScoreList scoreList;
    private String filePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Set file path to be used to read/write
        filePath = getApplicationContext().getFilesDir().getPath().toString() + "scores.ser";

        //Set circle layout
        final CircleLayout circleLayout = findViewById(R.id.circle_layout);
        adapter = new CircleButtonLayoutAdapter(getLayoutInflater());
        circleLayout.setAdapter(adapter);
        final View backgroundView = adapter.getBackground();

        //Initialise the date to today's date
        c = Calendar.getInstance();
        c.clear(Calendar.AM_PM);
        c.clear(Calendar.MINUTE);
        c.clear(Calendar.SECOND);
        c.clear(Calendar.MILLISECOND);
        c.set(Calendar.HOUR_OF_DAY,0);
        TextView dateText = backgroundView.findViewById(R.id.date_text_view);
        currDate = c.getTime();
        displayedDay = Integer.parseInt(dayFormatter.format(currDate));
        displayedMonth = Integer.parseInt(monthFormatter.format(currDate));
        displayedYear = Integer.parseInt(yearFormatter.format(currDate));
        dateText.setText(dateFormatter.format(currDate));

        //Set listeners for all buttons
        Button dateButton = backgroundView.findViewById(R.id.date_button);
        dateButton.setOnClickListener(this);
        for(int i = 0; i<adapter.getCount(); i++) {
            final View view = adapter.getViewAt(i);
            String name = adapter.getNameAt(i);
            TextView buttonName = view.findViewById(R.id.button_name);
            buttonName.setText(name);
            Button button = view.findViewById(R.id.custom_button);
            button.setOnClickListener(this);
            button.setTag(name);
            button.setId(i);
        }

        //Initialise score list
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filePath));
            this.scoreList = (ScoreList) ois.readObject();
            ois.close();
            System.out.println("file found, opened current scorelist");
        } catch (FileNotFoundException error) {
            this.scoreList = new ScoreList(this.currDate);
            System.out.println("File not found, created new score list");
        } catch (Exception error) {
            error.printStackTrace();

            System.out.println("other error");
        }
        //Set all points text fields

        scoreList.scoreListDateChanged(currDate);
        updateFields();
    }

    public void updateFields() {
        ArrayList<Integer> scoresArray = scoreList.getScore().getAllScores();
        for(int i = 0; i<adapter.getCount(); i++) {
            final View view = adapter.getViewAt(i);
            TextView pointsText = view.findViewById(R.id.button_point_score);
            pointsText.setText("" + scoresArray.get(i));
        }

        View backgroundView = adapter.getBackground();
        TextView totalScoreText = backgroundView.findViewById(R.id.todays_score);
        String totalScoreString = Integer.toString(scoreList.getScore().getTotalScore());
        totalScoreText.setText(totalScoreString);
    }

    @Override
    public void onClick(View view) {
        FragmentManager fragMan = getFragmentManager();
        SingleInputDialog singleInput = new SingleInputDialog();
        DoubleInputDialog doubleInput = new DoubleInputDialog();
        int currentValue;
        switch (view.getId()) {
            //Change date button
            case R.id.date_button:
                DialogFragment newFragment;
                newFragment = new DatePickerFragment();
                newFragment.show(fragMan, "datePicker");
                break;
            //Alcohol button
            case 0:
                currentValue = scoreList.getScore().getAlcoholUnit();
                singleInput.setDetails("Alcohol", "Enter number of units: ", currentValue, 0, 20, 1, 0);
                singleInput.show(fragMan, "alcoholInput");
                break;
            //Bloods button
            case 1:
                int systolic = scoreList.getScore().getSystolic();
                int diastolic = scoreList.getScore().getDiastolic();
                doubleInput.setDetails("Bloods", view.getId());
                doubleInput.setFirstDetails("Systolic(mm Hg): ", systolic, 200, 60, 5);
                doubleInput.setSecondDetails("Diastolic(mm Hg): ", diastolic, 120, 40, 2);
                doubleInput.show(fragMan, "bloodsInput");
                break;
            //De-Stress button
            case 2:
                currentValue = scoreList.getScore().getStressLevel();
                singleInput.setDetails("De-Stress", "Enter current stress level \n0: no stress \n10:very stressed", currentValue, 0, 10, 1, 2);
                singleInput.show(fragMan, "deStressInput");
                break;
            //De-Tox button
            case 3:
                currentValue = scoreList.getScore().getNoCigarettes();
                singleInput.setDetails("De=Tox", "Enter no of cigarettes today:", currentValue, 0, 30, 1, 3);
                singleInput.show(fragMan, "deToxInput");
                break;
            //Diet button
            case 4:
                currentValue = scoreList.getScore().getDietLevel();
                singleInput.setDetails("Diet", "10: not kept to diet \n0: kept to diet perfect", currentValue, 0, 10, 1, 4);
                singleInput.show(fragMan, "dietInput");
                break;
            //Exercise button
            case 5:
                int steps = scoreList.getScore().getSteps();
                int mins = scoreList.getScore().getMins();
                doubleInput.setDetails("Exercise", view.getId());
                doubleInput.setFirstDetails("Enter Steps: ", steps, 25000, 0, 200);
                doubleInput.setSecondDetails("Enter mins exercise: ", mins, 80, 0, 5);
                doubleInput.show(fragMan, "exerciseInput");
                break;
            //5 a Day button
            case 6:
                currentValue = scoreList.getScore().getNoFruitVeg();
                singleInput.setDetails("5 a Day", "Enter no of fruit/veg today:", currentValue, 0, 10, 1, 6);
                singleInput.show(fragMan, "fiveADayInput");
                break;
            //Happy Hours button
            case 7:
                currentValue = scoreList.getScore().getNoHappyHours();
                singleInput.setDetails("Happy Hours", "Enter no of fun hours today:", currentValue, 0, 10, 1, 7);
                singleInput.show(fragMan, "happyHoursInput");
                break;
            //Sleep button
            case 8:
                currentValue = scoreList.getScore().getNoSleepHours();
                singleInput.setDetails("Sleep", "Enter no of hours sleep:", currentValue, 0, 15, 1, 8);
                singleInput.show(fragMan, "fiveADayInput");
                break;
            //Sugar and Salt button
            case 9:
                int sugar = scoreList.getScore().getSugar();
                int salt = scoreList.getScore().getSalt();
                doubleInput.setDetails("Sugar and Salt", view.getId());
                doubleInput.setFirstDetails("Enter grams of sugar: ", sugar, 100, 0, 1);
                doubleInput.setSecondDetails("Enter grams of salt: ", salt, 50, 0, 1);
                doubleInput.show(fragMan, "sugarSaltInput");
                break;
            //Water button
            case 10:
                currentValue = scoreList.getScore().getNoGlasses();
                singleInput.setDetails("Water", "Enter no of glasses of water today:", currentValue, 0, 10, 1, 10);
                singleInput.show(fragMan, "waterInput");
                break;
            //Weight button
            case 11:
                currentValue = scoreList.getScore().getBmi();
                singleInput.setDetails("Weight", "Enter BMI:", currentValue, 0, 50, 1, 11);
                singleInput.show(fragMan, "weightInput");
                break;

            default:
                Toast.makeText(this,"Error button not found", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    public void onDialogOK (NumberPicker numberPickerOne, NumberPicker numberPickerTwo, int id) {
        int pickerOneIndex;
        int pickerTwoIndex;
        int pickerOneValue;
        int pickerTwoValue;
        String[] pickerOneArray;
        String[] pickerTwoArray;
        switch (id) {
            //Alcohol function
            case 0:
                scoreList.getScore().calculateAlcoholScore(numberPickerOne.getValue());
                break;

            //Bloods function
            case 1:
                pickerOneIndex = numberPickerOne.getValue();
                pickerOneArray = numberPickerOne.getDisplayedValues();
                pickerOneValue = Integer.parseInt(pickerOneArray[pickerOneIndex]);

                pickerTwoIndex = numberPickerTwo.getValue();
                pickerTwoArray = numberPickerTwo.getDisplayedValues();
                pickerTwoValue = Integer.parseInt(pickerTwoArray[pickerTwoIndex]);

                scoreList.getScore().calculateBloodsScore(pickerOneValue, pickerTwoValue);
                break;

            //De-Stress function
            case 2:
                scoreList.getScore().calculateDeStressScore(numberPickerOne.getValue());
                break;

            //De-Tox function
            case 3:
                scoreList.getScore().calculateDeToxScore(numberPickerOne.getValue());
                break;

            //Diet function
            case 4:
                scoreList.getScore().calculateDietScore(numberPickerOne.getValue());
                break;

            //Exercise function
            case 5:
                pickerOneIndex = numberPickerOne.getValue();
                pickerOneArray = numberPickerOne.getDisplayedValues();
                pickerOneValue = Integer.parseInt(pickerOneArray[pickerOneIndex]);

                pickerTwoIndex = numberPickerTwo.getValue();
                pickerTwoArray = numberPickerTwo.getDisplayedValues();
                pickerTwoValue = Integer.parseInt(pickerTwoArray[pickerTwoIndex]);
                scoreList.getScore().calculateExerciseScore(pickerOneValue, pickerTwoValue);
                break;

            //5 a Day function
            case 6:
                scoreList.getScore().calculateFivaADayScore(numberPickerOne.getValue());
                break;

            //Happy Hours function
            case 7:
                scoreList.getScore().calculateHappyHoursScore(numberPickerOne.getValue());
                break;

            //Sleep function
            case 8:
                scoreList.getScore().calculateSleepScore(numberPickerOne.getValue());
                break;

            //Sugar and Salt function
            case 9:
                pickerOneValue = numberPickerOne.getValue();
                pickerTwoValue = numberPickerTwo.getValue();
                scoreList.getScore().calculateSugarAndSaltScore(pickerOneValue, pickerTwoValue);
                break;

            //Water function
            case 10:
                scoreList.getScore().calculateWaterScore(numberPickerOne.getValue());
                break;

            //Weight function
            case 11:
                scoreList.getScore().calculateWeightScore(numberPickerOne.getValue());
                break;

            default:
                Toast.makeText(this,"Error on calculate", Toast.LENGTH_SHORT).show();
                break;
        }
        updateFields();
        save();
    }

    //Sets date from users input values to date picker.
    public void onDateSet(int selectedDay, int selectedMonth, int selectedYear){
        String dateString = "" + selectedDay + "/" + selectedMonth + "/" + selectedYear;
        try {
            currDate = dateFormatter.parse(dateString);
            TextView dateView = findViewById(R.id.date_text_view); // add (TextView) if breaks
            dateView.setText(dateFormatter.format(currDate));
            displayedDay = selectedDay;
            displayedMonth = selectedMonth;
            displayedYear = selectedYear;
            scoreList.scoreListDateChanged(currDate);
            updateFields();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    //Saves objects
    public void save() {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filePath));
            oos.writeObject(this.scoreList);
            oos.close();
        } catch (Exception error) {
            System.out.println("Error on saving");
            error.printStackTrace();
        }
    }
}
