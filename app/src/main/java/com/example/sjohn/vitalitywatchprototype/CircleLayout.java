package com.example.sjohn.vitalitywatchprototype;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by sjohn on 02/11/2017.
 */

public class CircleLayout extends RelativeLayout {

    public float layoutWidth;
    public float layoutHeight;
    public float viewWidth;
    public float viewHeight;
    public float layoutCenter_x;
    public float layoutCenter_y;
    public float radius;
    public float diameter;
    private double intervalAngle;
    private double startAngle;
    private double marginLeft;
    private double marginTop;
    private CircleButtonLayoutAdapter adapter;

    public  CircleLayout (Context context) {
        super(context);
        init();
    }
    public CircleLayout (Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    public CircleLayout (Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {       //Initialise and get real height and width

        post(new Runnable() {
            @Override
            public void run() {
                Log.e("CircularListView", "get layout width and height");
                layoutWidth = getWidth();
                layoutHeight = getHeight();
                layoutCenter_x = layoutWidth / 2;
                layoutCenter_y = layoutHeight / 3.1f;
                radius = (layoutWidth / 2.6f);
            }
        });
    }

    public void setAdapter(CircleButtonLayoutAdapter adapter) {
        this.adapter = adapter;
        setCircleLayoutBackground();
        setButtonPositions();
    }

    public void setCircleLayoutBackground() {
        final View view = adapter.getBackground();

        if (view.getParent() == null) {
            addView(view);
        }
        view.post(new Runnable() {
            @Override
            public void run() {
                diameter = (radius*2.5f) ;
                LayoutParams params = new LayoutParams(0,0);
                params.height = ((int) diameter);
                params.width = ((int) diameter);
                params.setMargins((int) (layoutCenter_x - diameter/2), (int) (layoutCenter_y - diameter/2), 0, 0);
                view.setLayoutParams(params);
            }
        });
    }

    private void setButtonPositions() {
        int viewCount = adapter.getCount();
        startAngle = -Math.PI/2f;
        intervalAngle = 2f*Math.PI/ (double) viewCount;

        for (int i = 0; i < adapter.getCount(); i++) {
            final View view = adapter.getViewAt(i);
            final int idx = i;
            final String name = adapter.getNameAt(i);
            if(view.getParent() == null) {
                addView(view);
            }

            view.post(new Runnable() {
                @Override
                public void run() {
                    viewWidth = view.getWidth();
                    viewHeight = view.getHeight();
                    if (view.getId()==0) {
                        intervalAngle = startAngle;
                    } else {
                        intervalAngle = startAngle + (idx *((2*Math.PI)/ adapter.getCount()));
                    }
                    marginLeft = (layoutCenter_x - (viewWidth/2) + (radius * Math.cos(intervalAngle)));
                    marginTop = layoutCenter_y - (viewHeight/2) + (radius * Math.sin(intervalAngle));
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                    params.setMargins( (int) marginLeft, (int) marginTop, 0, 0);
                }
            });
        }
    }
}
